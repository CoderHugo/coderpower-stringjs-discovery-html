var S = require('string');

module.exports = function html(foo) {
    foo.decodeHTMLEntities = S('Coder &amp; power').decodeHTMLEntities().s;
    console.log('Decode HTML entities', foo.decodeHTMLEntities);

    foo.stripTags = S('<p>Remove all <b>tags</b> please</p>').stripTags().s;
    console.log('Strip tags', foo.stripTags);

    foo.wrapHTML = S('This is a paragraph').wrapHTML('p').s;
    console.log('Wrap text with HTML', foo.wrapHTML);

    return foo;
};
